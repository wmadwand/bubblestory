﻿using UnityEngine;
using DTools.ResourcesPool;
using System.Collections;
using UnityEngine.UI;

public class Boom : PoolObject
{
	[SerializeField] float _timeWaitBeforeDestroy;
	[SerializeField] Text _scoreText;

	//---------------------------------------------------------

	public void SetScore(int value)
	{
		_scoreText.text = $"+{value}";
	}

	//---------------------------------------------------------

	private void OnEnable()
	{
		StartCoroutine(WaitBeforeReturnToPool());
	}

	protected override void OnTakenFromPool()
	{
		gameObject.SetActive(true);
	}

	private IEnumerator WaitBeforeReturnToPool()
	{
		yield return new WaitForSeconds(_timeWaitBeforeDestroy);

		ReturnToPool();
	}
}