﻿using System.Collections;
using UnityEngine;
using DTools.ResourcesPool;

public class BubbleSpawnManager : PoolObject
{
	[SerializeField] private GameObject _bubblePrefab;
	[SerializeField] private float _timeSpawn;

	private bool _isSpawning;
	private Vector3 _spawnPosition;

	private const string BUBBLE_PATH = "Bubble";
	private const int BUBBLE_CACHE_START_SIZE = 20;
	private Pool<Bubble> _bubblePool;

	//---------------------------------------------------------

	private void Awake()
	{
		Game.OnStart += Game_OnStart;
		Game.OnStop += Game_OnStop;

		_bubblePool = PoolManager.GetPool<Bubble>(BUBBLE_PATH);
		_bubblePool.Prewarm(BUBBLE_CACHE_START_SIZE);
	}

	private void OnDestroy()
	{
		Game.OnStart -= Game_OnStart;
		Game.OnStop -= Game_OnStop;
	}

	private void Game_OnStart()
	{
		_isSpawning = true;
		StartCoroutine(SpawnRoutine());
	}

	private void Game_OnStop()
	{
		_isSpawning = false;
	}

	private IEnumerator SpawnRoutine()
	{
		while (_isSpawning)
		{
			_spawnPosition = new Vector3(Random.Range(-8, 8), -12, Random.Range(-3, 4));
			_bubblePool.GetObject(_spawnPosition, Quaternion.identity);

			yield return new WaitForSeconds(_timeSpawn);
		}
	}
}