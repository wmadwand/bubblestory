﻿using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoSingleton<ScoreManager>
{
	public int Score { get; private set; }

	[SerializeField] private Text _scoreText;

	//---------------------------------------------------------

	private void Awake()
	{
		Bubble.OnClick += Logic_OnClick;
		Game.OnStart += GameController_OnGameStart;
	}

	private void OnDestroy()
	{
		Bubble.OnClick -= Logic_OnClick;
		Game.OnStart -= GameController_OnGameStart;
	}

	private void GameController_OnGameStart()
	{
		ResetScore();
	}

	private void Logic_OnClick(int scoreValue)
	{
		Score += scoreValue;
		UpdateText();
	}

	private void ResetScore()
	{
		Score = 0;
		UpdateText();
	}

	private void UpdateText()
	{
		_scoreText.text = $"Score: {Score}";
	}
}