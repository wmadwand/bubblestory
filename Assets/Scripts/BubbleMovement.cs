﻿using UnityEngine;

public class BubbleMovement : MonoBehaviour
{
	[SerializeField] private float _speed;
	[SerializeField] float _baseSpeedTimerFactor = 0;

	private float _baseSpeedTimer = 0;
	private Rigidbody _rigidbody;

	//---------------------------------------------------------

	public void SetSpeed(float bubbleSize)
	{
		_speed = _speed / bubbleSize;
	}

	//---------------------------------------------------------

	private void Awake()
	{
		_rigidbody = GetComponentInChildren<Rigidbody>();

		Timer.OnChange += Timer_OnChange;
	}

	private void FixedUpdate()
	{
		_rigidbody.velocity = transform.up * (_speed * _baseSpeedTimer) * Time.deltaTime;
	}

	private void OnDestroy()
	{
		Timer.OnChange -= Timer_OnChange;
	}

	private void Timer_OnChange(float timerValue)
	{
		_baseSpeedTimer = _baseSpeedTimerFactor / (timerValue > 0 ? timerValue : 1);
	}
}