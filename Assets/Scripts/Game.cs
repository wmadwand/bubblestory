﻿using System;
using UnityEngine;

public class Game : MonoBehaviour
{
	public static event Action OnStart;
	public static event Action OnStop;

	//---------------------------------------------------------

	public void StartGame()
	{
		OnStart?.Invoke();
	}

	//---------------------------------------------------------

	private void Awake()
	{
		Timer.OnFinish += Timer_OnFinish;
	}

	private void OnDestroy()
	{
		Timer.OnFinish -= Timer_OnFinish;
	}

	private void Timer_OnFinish()
	{
		OnStop?.Invoke();
	}
}