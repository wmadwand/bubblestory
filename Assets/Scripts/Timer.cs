﻿using System;
using UnityEngine;
using UnityEngine.UI;

//TODO: exclude TimerView class for subscribing to Time class events. So that logic and view could be separated.
public class Timer : MonoBehaviour
{
	public static event Action OnStart;
	public static event Action<float> OnChange;
	public static event Action OnFinish;

	public float CurrentTime => _currentTime;

	[SerializeField] private float _startTime;
	[SerializeField] private Text _text;

	private float _currentTime = 0;
	private bool _isPlay;

	//---------------------------------------------------------

	private void OnEnable()
	{
		UpdateText();
	}

	private void Awake()
	{
		Game.OnStart += GameController_OnGameStart;
	}

	private void Update()
	{
		if (_isPlay)
		{
			if (_currentTime > 0)
			{
				_currentTime -= Time.deltaTime;
				OnChange?.Invoke(_currentTime);
				UpdateText();
			}
			else
			{
				Stop();
			}
		}
	}

	private void OnDestroy()
	{
		Game.OnStart -= GameController_OnGameStart;
	}

	private void GameController_OnGameStart()
	{
		Play();
	}

	private void Play()
	{
		_isPlay = true;

		_currentTime = _startTime;
		UpdateText();

		OnStart?.Invoke();
	}

	private void Stop()
	{
		_isPlay = false;
		_currentTime = 0;
		UpdateText();

		OnFinish?.Invoke();
	}

	private void UpdateText()
	{
		_text.text = $"Time: {_currentTime.ToString("00")}";
	}
}