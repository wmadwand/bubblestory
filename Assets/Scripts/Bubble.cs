﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using DTools.ResourcesPool;

[RequireComponent(typeof(BubbleMovement))]
[RequireComponent(typeof(BubbleScore))]
public class Bubble : PoolObject, IPointerClickHandler
{
	public static event Action<int> OnClick;

	[SerializeField] private GameObject _boomEffect;

	private float _size = 1;
	private Renderer _renderer;
	private BubbleMovement _movement;
	private BubbleScore _score;
	private Boom _boom;

	private const string BOOM_PATH = "Boom";
	private const int BOOM_CACHE_START_SIZE = 20;
	private static Pool<Boom> _boomPool;

	//---------------------------------------------------------

	protected override void OnTakenFromPool()
	{
		gameObject.SetActive(true);
	}

	private void Awake()
	{
		_renderer = GetComponent<Renderer>();
		_movement = GetComponent<BubbleMovement>();
		_score = GetComponent<BubbleScore>();

		if (_boomPool == null)
		{
			_boomPool = PoolManager.GetPool<Boom>(BOOM_PATH);
			_boomPool.Prewarm(BOOM_CACHE_START_SIZE);
		}
	}

	private void Start()
	{
		SetView();

		_score.SetValue(_size);
		_movement?.SetSpeed(_size);
	}

	void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
	{
		OnClick?.Invoke(_score.Value);

		Boom _boom =_boomPool.GetObject(transform.position, Quaternion.identity);
		_boom.SetScore(_score.Value);

		this.ReturnToPool();
	}

	private void SetView()
	{
		_size = UnityEngine.Random.Range(1f, 4f);
		transform.localScale = new Vector3(_size, _size, _size);
		_renderer.material.color = UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
	}
}