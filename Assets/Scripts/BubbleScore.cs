﻿using System;
using UnityEngine;

public class BubbleScore : MonoBehaviour
{
	public int Value => _value;
	[SerializeField] private int _value = 100;

	public void SetValue(float bubbleSize)
	{
		_value = (int)Math.Floor(_value / bubbleSize);
	}
}